#include <iostream>
#include <algorithm>
#include "BookInfo.h"
#include "Book.h"
#include "Library.h"

int main() {

    Library library;

    vector<string> authors;
    authors.emplace_back("Kiro Breika");
    // authors.emplace_back("Qni Mihaylov");

    Book book("Once Upon a time", authors, true, 0);

    vector<string> authors2;
    authors2.emplace_back("Dora Petkova");
    authors2.emplace_back("Asen Kovec");

    Book book2("Pesho kirkata", authors2, false, 20110608);
    library.addBook(book);
    library.addBook(book2);


    while (true) {
        cout << "Изберте опция:\n";
        cout << "1.Извежда информацията за наличните в момента книги, подредени по азбучен ред на авторите.\n";
        cout << "2.Извежда заглавията на книгите, които са заети на зададена дата.(yyyy/mm/dd)\n";
        cout << "3.Записва във файл информацията за книгите, които са с повече от един автора.\n";
        cout << "4.Изход\n";
        int option;
        cin >> option;
        if (option == 4) {
            break;
        }
        switch (option) {
            case 1:
                library.getBooksByAuthorAsc();
                break;
            case 2:
                cout << "Въведете дата във формат yyyy/mm/dd\n";
                long date;
                cin >> date;
                library.printBooksTakenOnDate(date);
                break;
            case 3:
                cout << library;
                break;
            default:
                cout << "Грешна опция\nОпитай пак:";
                break;
        }
    }
    return 0;
}