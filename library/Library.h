//
// Created by yani on 9.11.19 г..
//

#ifndef LIBRARY_LIBRARY_H
#define LIBRARY_LIBRARY_H
using namespace std;

#include <fstream>
#include "iostream"
#include "Book.h"

class Library {
private:
    int titleCount;
    vector<string> titles;
    vector<Book> books;
public:
    Library();

    void addBook(Book book);

    ~Library();

    void getBooksByAuthorAsc();

    void printBooksTakenOnDate(int long dateTaken);

    friend ostream &operator<<(ostream &os, const Library &library);
};

Library::Library() {
    titleCount = 0;
}

void Library::addBook(Book book) {
    titles.emplace_back(book.getTitle());
    books.emplace_back(book);
    titleCount++;
}

void Library::getBooksByAuthorAsc() {
    vector<string> allAuthors;
    for (Book b : books) {
        for (basic_string<char> &s : b.getAuthors()) {
            allAuthors.emplace_back(s);
        }
    }

    sort(allAuthors.begin(), allAuthors.end());

    vector<string> finalTitles;
    for (string &author :allAuthors) {
        for (Book b : books) {
            string title = b.containsAuthor(author);
            if (!title.empty()) {
                if (!(find(finalTitles.begin(), finalTitles.end(), title) != finalTitles.end())) {
                    finalTitles.emplace_back(title);
                }
            }
        }
    }

    for (const string& title: finalTitles) {
        cout << title + "\n";
    }
}

void Library::printBooksTakenOnDate(long dateTaken) {
    for (Book b:books) {
        if (b.getDateTaken() == dateTaken) {
            cout << b.getTitle();
        }
    }
}

ostream &operator<<(ostream &os, const Library &library) {
    ofstream file;
    file.open("3.information");

    for (Book b : library.books) {
        if (b.getAuthorCount() > 1) {
            file << "Title: " + b.getTitle() + "\n";
            file << "Authors: ";
            for (const string &name : b.getAuthors()) {
                file << name + "  |  ";
            }
            file << "\nIs book available: " + b.isBookAvailable() + "\n";
            file << "Date taken: ";
            file << b.getDateTakenAsDateFormatted() + "\n";
            file << "\n";
        }
    }

    file.close();
    return os;
}

Library::~Library() = default;


#endif //LIBRARY_LIBRARY_H
