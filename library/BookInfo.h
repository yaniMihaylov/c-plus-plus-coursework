//
// Created by yani on 9.11.19 г..
//

#ifndef LIBRARY_BOOKINFO_H
#define LIBRARY_BOOKINFO_H
using namespace std;

#include <utility>
#include <vector>
#include "iostream"

class BookInfo {
private:
   vector<string> authors;
   bool isPresent;
   long dateTaken;
public:
    BookInfo(vector<string> inAuthors, bool inIsPresent, long inDateTaken);
    ~BookInfo();
    long getDateTaken();
    int getAuthorCount();

    vector <string> getAuthors();

    string isBookAvailable();

    string getDateTakenAsDateFormatted();
};
BookInfo::~BookInfo() = default;

BookInfo::BookInfo(vector<string> inAuthors, bool inIsPresent, long inDateTaken) {
    authors = std::move(inAuthors);
    if (inDateTaken != 0) {
        isPresent = false;
    }

    if (isPresent) {
        dateTaken = 0;
    } else {
        dateTaken = inDateTaken;
    }
}

long BookInfo :: getDateTaken() {
    return dateTaken;
}

int BookInfo :: getAuthorCount() {
    return authors.size();
}

vector<string> BookInfo :: getAuthors(){
    return authors;
}

string BookInfo :: isBookAvailable() {
    if (isPresent) {
        return "yes";
    }

    return "no";
}

string BookInfo :: getDateTakenAsDateFormatted() {

    string date = to_string(dateTaken);

    if (date.size() == 1){
        return  "0";
    }
    string year = date.substr(0,4);
    string month = date.substr(4,6);
    string day = date.substr(6,8);
    string formattedDate = year + "/" + month.substr(0,2) + "/" + day;

    return formattedDate;
}

#endif //LIBRARY_BOOKINFO_H
