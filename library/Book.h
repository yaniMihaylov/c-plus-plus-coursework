//
// Created by yani on 9.11.19 г..
//

#ifndef LIBRARY_BOOK_H
#define LIBRARY_BOOK_H
using namespace std;

#include <utility>

#include "BookInfo.h"
#include "iostream"
class Book : public BookInfo {
private:
    string title;
public:
    Book(string inTitle, vector<string> inAuthors, bool inIsPresent, long inDateTaken);
    string getTitle();
    ~Book();

    string containsAuthor(string authorName);
};

Book::Book(string inTitle, vector<string> inAuthors, bool inIsPresent,long inDateTaken) : BookInfo(std::move(inAuthors),inIsPresent,inDateTaken){
    title = std::move(inTitle);
}

string Book ::getTitle() {
    return title;
}

Book::~Book() {

}

string Book :: containsAuthor(string authorName) {
    vector<string> authors = getAuthors();

    if (!authors.empty()) {
        if (find(authors.begin(), authors.end(), authorName) != authors.end()) {
            return title;
        }
    }

    return "";
}
#endif //LIBRARY_BOOK_H
